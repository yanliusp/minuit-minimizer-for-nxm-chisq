/// \file
/// \ingroup tutorial_fit
/// \notebook -nodraw
/// Example on how to use the new Minimizer class in ROOT
///  Show usage with all the possible minimizers.
/// Minimize the Rosenbrock function (a 2D -function)
/// This example is described also in
/// http://root.cern.ch/drupal/content/numerical-minimization#multidim_minim
/// input : minimizer name + algorithm name
/// randomSeed: = <0 : fixed value: 0 random with seed 0; >0 random with given seed
///
/// \macro_code
///
/// \author Lorenzo Moneta

#include "Math/Minimizer.h"
#include "Math/Factory.h"
#include "Math/Functor.h"
#include "TRandom2.h"
#include "TError.h"
#include <iostream>

using namespace std;

void RealToComplexFFT(const vector<double>& pulsevector, vector<complex<double>>& outComp)
{
    int n = pulsevector.size();
    if(n == 0)
    {
      cerr <<"PulseTools::RealToComplexFFT - ERROR! empty pulse passed into this function" << endl;
      exit(1);
    }

    double *pvector = new double[pulsevector.size()];
    double re,im;
    int i;

   //copying std vector back into array to pass to FFT (to mirror what is done in PulseTools)
   for (i=0;i<(int)pulsevector.size();i++){
      pvector[i] = pulsevector[i];
   }

   TVirtualFFT *fftr2c = TVirtualFFT::FFT(1,&n,"R2C ES");
   fftr2c->SetPoints(pvector);
   fftr2c->Transform();
   fftr2c->GetPointComplex(0,re,im);
   //TComplex tempCopy(re/sqrt((double)n), im/sqrt((double)n));
   complex<double> tempCopy(re/sqrt((double)n), im/sqrt((double)n));
   outComp.push_back(tempCopy);

   for(i=1; i<n ;i++)
   {
      fftr2c->GetPointComplex(i,re,im);
      //TComplex tempCopy(re/sqrt((double)n), im/sqrt((double)n));
      complex<double> tempCopy(re/sqrt((double)n), im/sqrt((double)n));
      outComp.push_back(tempCopy);
   }

   //done! so delete new'd objects
   delete[] pvector;
   delete fftr2c;

   return;

}


void RealToComplexFFT(const vector<double>& pulsevector, vector<TComplex>& outComp)
{
    int n = pulsevector.size();
    if(n == 0)
    {
      cerr <<"PulseTools::RealToComplexFFT - ERROR! empty pulse passed into this function" << endl;
      exit(1);
    }

    double *pvector = new double[pulsevector.size()];
    double re,im;
    int i;

   //copying std vector back into array to pass to FFT (to mirror what is done in PulseTools)
   for (i=0;i<(int)pulsevector.size();i++){
      pvector[i] = pulsevector[i];
   }

   TVirtualFFT *fftr2c = TVirtualFFT::FFT(1,&n,"R2C ES");
   fftr2c->SetPoints(pvector);
   fftr2c->Transform();
   fftr2c->GetPointComplex(0,re,im);
   TComplex tempCopy(re/sqrt((double)n), im/sqrt((double)n));
   outComp.push_back(tempCopy);

   for(i=1; i<n ;i++)
   {
      fftr2c->GetPointComplex(i,re,im);
      TComplex tempCopy(re/sqrt((double)n), im/sqrt((double)n));
      outComp.push_back(tempCopy);
   }

   //done! so delete new'd objects
   delete[] pvector;
   delete fftr2c;

   return;

}

double ChisqNxM(const double *amps )
{
  cout << "calling once.. " << endl;
  const Double_t amps0 = amps[0];
  const Double_t amps1 = amps[1];
  const Int_t eventNo = (Int_t)amps[2];

  //data 
  TFile* f1 = new TFile("minimizer_sampler.root", "READ");
  TH1D* t1Re = (TH1D*) f1->Get("sig1FFTRe");
  TH1D* t1Im = (TH1D*) f1->Get("sig1FFTIm");
  TH1D* t2Re = (TH1D*) f1->Get("sig2FFTRe");
  TH1D* t2Im = (TH1D*) f1->Get("sig2FFTIm");
  TList* InvNoisePSD = (TList*) f1->Get("InvNoisePSD");

  TList* fakePulses = (TList*) f1->Get("FakePulses");
  TList* list1 = (TList*) fakePulses->At(eventNo);
  //TList* list2 = (TList*) fakePulses->At(1);
  TH1D* s1Time = (TH1D*) list1->At(0);
  TH1D* s2Time = (TH1D*) list1->At(1);
  //TH1D* s1Time = (TH1D*) f1->Get("hist1");
  //TH1D* s2Time = (TH1D*) f1->Get("hist2");
  //TH1D* t1Time = (TH1D*) f1->Get("tmpl3");
  //TH1D* t2Time = (TH1D*) f1->Get("tmpl4");
  vector<double> s1pulse, s2pulse;
  for (int c=1; c<= s1Time->GetNbinsX(); c++) { // TH1D starts at 1
    s1pulse.push_back(s1Time->GetBinContent(c));
    s2pulse.push_back(s2Time->GetBinContent(c));
  }

  vector<TComplex> s1FFT, s2FFT;
  //vector<complex<double>> s1FFT, s2FFT;
  RealToComplexFFT(s1pulse, s1FFT);
  RealToComplexFFT(s2pulse, s2FFT);

  vector<double> s1Real, s1Imag, s2Real, s2Imag;
  for (int c=0; c< s1FFT.size(); c++) {
    s1FFT[c] *= 0.00126491;
    s2FFT[c] *= 0.00126491;
    //s1Real.push_back(real(s1FFT[c])); //s1Real.push_back(s1FFT[c].Re());
    //s1Imag.push_back(imag(s1FFT[c])); //s1Imag.push_back(s1FFT[c].Im());
    //s2Real.push_back(real(s2FFT[c])); //s2Real.push_back(s2FFT[c].Re());
    //s2Imag.push_back(imag(s2FFT[c])); //s2Imag.push_back(s2FFT[c].Im());
    s1Real.push_back(s1FFT[c].Re());
    s1Imag.push_back(s1FFT[c].Im());
    s2Real.push_back(s2FFT[c].Re());
    s2Imag.push_back(s2FFT[c].Im());
  }

  const Int_t nrPnts = 4;
  double ans = 0;
  for (int binCtr=1; binCtr<=t1Re->GetNbinsX(); binCtr++) {// loop through all bins
    vector<Double_t> s = {s1Real[binCtr-1], s1Imag[binCtr-1], s2Real[binCtr-1], s2Imag[binCtr-1]};
    //vector<Double_t> s = {t1Re->GetBinContent(binCtr)*3.5, t1Im->GetBinContent(binCtr)*3.5, t2Re->GetBinContent(binCtr)*2, t2Im->GetBinContent(binCtr)*2};
    //vector<Double_t> s = {10., 12., 15., 9.};
    vector<Double_t> tmpl = {t1Re->GetBinContent(binCtr), t1Im->GetBinContent(binCtr), t2Re->GetBinContent(binCtr), t2Im->GetBinContent(binCtr)};
    //vector<Double_t> tmpl = {1.0, 1.2, 3.0, 1.8};
    for (int i=0; i<nrPnts; i++) {
      if (i<2) tmpl[i] = tmpl[i]*amps0;
      else tmpl[i] = tmpl[i]*amps1;
    }

    TMatrixD s_vec(1,nrPnts);
    for(int m = 0; m < nrPnts; m++){
      s[m] -= tmpl[m];
      s_vec[0][m]= s[m];
    }

    TMatrixD s_vec_T(nrPnts, 1);
    s_vec_T.Transpose(s_vec);
    //s_vec_T.Print();

    TMatrixD cov(nrPnts, nrPnts);
    //for(int m = 0; m < nrPnts; m++){
    //  for(int n = 0; n < nrPnts; n++){
    //    if (m==n) cov[m][n] = 1.0;
    //    else cov[m][n]= 0.0; // no correlation
    //  }
    //}
    
    for(int m = 0; m < nrPnts; m++){
      TList *l1 = (TList*)InvNoisePSD->At(m);
      for(int n = 0; n < nrPnts; n++){
        TH1D *h1 = (TH1D*)l1->At(n);
        cov[m][n]= (double) h1->GetBinContent(1000); // no correlation
      }
    }


    TMatrixD prod1(1, nrPnts);
    prod1.Mult(s_vec, cov);
    TMatrixD prod2(1, 1);
    prod2.Mult(prod1, s_vec_T);

    ans += prod2[0][0];

  }
  cout << ans << endl;
  return ans;
}

int NumericalMinimization(const char * minName = "Minuit2",
                          const char *algoName = "" ,
                          int randomSeed = -1,
                          int evn)
{
   // create minimizer giving a name and a name (optionally) for the specific
   // algorithm
   // possible choices are:
   //     minName                  algoName
   // Minuit /Minuit2             Migrad, Simplex,Combined,Scan  (default is Migrad)
   //  Minuit2                     Fumili2
   //  Fumili
   //  GSLMultiMin                ConjugateFR, ConjugatePR, BFGS,
   //                              BFGS2, SteepestDescent
   //  GSLMultiFit
   //   GSLSimAn
   //   Genetic
   ROOT::Math::Minimizer* minimum =
      ROOT::Math::Factory::CreateMinimizer(minName, algoName);

   // set tolerance , etc...
   minimum->SetMaxFunctionCalls(1000000); // for Minuit/Minuit2
   minimum->SetMaxIterations(10000);  // for GSL
   //minimum->SetTolerance(0.001);
   minimum->SetTolerance(1e-10);
   minimum->SetPrintLevel(1);

   // create function wrapper for minimizer
   // a IMultiGenFunction type
   ROOT::Math::Functor f(&ChisqNxM,3);
   double step[2] = {0.01,0.01};
   // starting point

   double variable[2] = { -1.,1.2};
   if (randomSeed >= 0) {
      TRandom2 r(randomSeed);
      variable[0] = r.Uniform(-20,20);
      variable[1] = r.Uniform(-20,20);
   }

   minimum->SetFunction(f);

   // Set the free variables to be minimized !
   minimum->SetVariable(0,"amp0",variable[0], step[0]);
   minimum->SetVariable(1,"amp1",variable[1], step[1]);
   minimum->SetFixedVariable(2,"event number", 0);

   // do the minimization
   minimum->Minimize();

   const double *xs = minimum->X();
   cout << xs[0] + xs[1] << "           YL            " << endl;
   std::cout << "Minimum: f(" << xs[0] << "," << xs[1] << "): "
             << minimum->MinValue()  << std::endl;

   // expected minimum is 0
   if ( minimum->MinValue()  < 1.E-4  && f(xs) < 1.E-4)
      std::cout << "Minimizer " << minName << " - " << algoName
                << "   converged to the right minimum" << std::endl;
   else {
      std::cout << "Minimizer " << minName << " - " << algoName
                << "   failed to converge !!!" << std::endl;
      Error("NumericalMinimization","fail to converge");
   }

   return 0;
}

int test() {

NumericalMinimization(,,,0);
}
